require "httparty"
require "faker"
require "rspec"
require "cucumber"

#Ignore SSL certificate for API requests
HTTParty::Basement.default_options.update(verify: false)

$profile = ENV['PROFILE']

api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]
