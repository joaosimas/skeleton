# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que eu queira recuperar um post") do

	puts "INICIANDO RECUPERAÇÃO DO POST, ID 2"	

end






Quando("o método for get com um id existente") do

  	get_post

end






Então("me retorna o status 200 com o response body equivalente") do


	expect(@procurar_post.code).to eq 200


  	@parse_post = JSON.parse(@procurar_post.body, object_class: OpenStruct)
  	expect(@parse_post.title).to eq "qui est esse"

  	puts "\n\nPRINTANDO O TITULO DO POST\n\n"
  	puts @parse_post.title

end